SlamWorker.Processor = class Processor {
    constructor() {

    }

    malloc(typedArray) {
        var numBytes = typedArray.length * typedArray.BYTES_PER_ELEMENT;
        var ptr = Module._malloc(numBytes);

        let HEAP = (function() {
            if (typedArray instanceof Uint8Array) return Module.HEAPU8;
            else return Module.HEAPF32;
        })();

        let bytes = HEAP.subarray(ptr, ptr + numBytes);
        bytes.set(typedArray);
        return bytes;
    }

    free(heapBytes) {
        Module._free(heapBytes.byteOffset);
    }

    init({url, width, height}) {
        this.width = width;
        this.height = height;

        importScripts("http://localhost:3000/wasm/facelab.js");

        const _this = this;
        Module.onRuntimeInitialized = _ => {
            _this.pixels = _this.malloc(new Uint8Array(width * height * 4));
            _this.data = _this.malloc(new Float32Array(50));
            Module._facelab_init(width * height * 4);
        };
    }

    process({pixels}, id) {
        if (!this.pixels) return resolve({found: 0}, id);
        this.pixels.set(pixels);
        let found = Module._facelab_get_landmarks(this.width, this.height, this.pixels.byteOffset, this.data.byteOffset);
        resolve({found}, id);
    }
}
