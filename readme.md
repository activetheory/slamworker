# Server
- `cd server`
- `npm i`
- `npm start`

The server will run at `http://localhost:3000` and will automatically inject scripts found in `www/js`.
This project gives us a really clean and simple environment to work in, and eventually will be built for deployment in another codebase.

# Front-end
- `index.html` has some example code for setting up the webcam as a video element and passing it to SlamWorker.
- SlamWorker `main.js` initializes a neatly abstracted WebWorker which runs the `Processor` class on another thread.
- In `main.js` on the main thread, we create a loop which extracts the pixels from webcam and transfers them to the thread.
- `Processor` on the thread initializes the OpenCV wasm module and passes the incoming pixel data to it via shared memory in order to get a 1 or 0 if there is a face detected in the image.
