SlamWorker.Main = class Main {

    init(video) {
        this.thread = new SlamWorker.Thread(SlamWorker.Processor);
        this.thread.init({width: video.width/4, height: video.height/4});

        let canvas = document.createElement('canvas');
        let context = canvas.getContext('2d');
        canvas.width = video.width/4;
        canvas.height = video.height/4;

        const _this = this;
        setInterval(async _ => {
            context.drawImage(video, 0, 0, canvas.width, canvas.height);
            let pixels = context.getImageData(0, 0, canvas.width, canvas.height).data;
            let {found} = await _this.thread.process({pixels}, [pixels.buffer]);
            console.log(found);
        }, 1000 / 24);
    }

    uuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
            let r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    }

    createPromise() {
        const promise = new Promise((resolve, reject) => {
            this.temp_resolve = resolve;
            this.temp_reject = reject;
        });
        promise.resolve = this.temp_resolve;
        promise.reject = this.temp_reject;
        delete this.temp_resolve;
        delete this.temp_reject;
        return promise;
    }
}
