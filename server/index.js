const _ecstatic = require('ecstatic');
const _path = require('path');
const _http = require('http');
const _fs = require('fs-extra');
const _walkSync = require('klaw-sync');

const ROOT = _path.join('..', 'www');
const START = '<!-- START GENERATED FILES -->';
const END = '<!-- END GENERATED FILES -->';

const _middleware = _ecstatic({
    root: ROOT,
    cors: true,
    gzip: true,
    showDotfiles: false,
    humanReadable: true,
    cache: decideCache,
    mimeType: { 'application/wasm': ['wasm'] }
});

function decideCache(file) {
    if (!!~file.indexOf('.js') && !~file.indexOf('.json')) return 0;
    if (!!~file.indexOf('.css')) return 0;
    if (!!~file.indexOf('.html')) return 0;
    if (!!~file.indexOf('.glsl')) return 0;
    if (!!~file.indexOf('.vs')) return 0;
    if (!!~file.indexOf('.fs')) return 0;
    if (!!~file.indexOf('.wasm')) return 0;
    return 3600;
}

function onRequest(req) {
    if (req.url == '/' || req.url.includes('index.html')) {
        let indexPath = _path.join(ROOT, 'index.html');
        let html = _fs.readFileSync(indexPath).toString();
        let pre = html.split(START)[0];
        let post = html.split(END)[1];

        let scripts = '';
        let files = _walkSync(_path.join(ROOT, 'js'));
        files.forEach(obj => {
            if (obj.path.includes('.js')) {
                let scriptPath = obj.path.split('www')[1].replace('\\', '/').slice(1).trim();
                scripts += `<script src="${scriptPath}"></script>\n`;
            }
        });

        _fs.writeFileSync(indexPath, pre + START + '\n' + scripts + '\n' + END + post);
    }
    _middleware.apply(_middleware, arguments);
}

_http.createServer(onRequest).listen(3000);
