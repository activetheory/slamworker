SlamWorker.Thread = class Thread {
    constructor(_class) {
        this.msg = {};
        this.callbacks = {};

        const _this = this;

        let blob = new Blob([SlamWorker.Bootstrap]);
        this.worker = new Worker(URL.createObjectURL(blob));
        this.worker.postMessage({es6: `(${eval(_class)})`});
        this.worker.addEventListener('message', e => {
            let callback = _this.callbacks[e.data.id];
            if (callback) callback(e.data.message);
            delete _this.callbacks[e.data.id];
        });

        Object.getOwnPropertyNames(_class.prototype).forEach(name => {
            if (name == 'constructor') return;
            _this[name] = function(message = {}, callback, buffer) {
                let promise;

                if (Array.isArray(callback)) {
                    buffer = callback;
                    callback = undefined;
                }

                if (Array.isArray(buffer)) {
                    message = {msg: message, transfer: true};
                    message.buffer = buffer;
                }

                if (callback === undefined) {
                    promise = SlamWorker.instance.createPromise();
                    callback = promise.resolve;
                }

                _this.send(name, message, callback);
                return promise;
            };
        });
    }

    send(name, message, callback) {
        if (typeof name === 'string') {
            var fn = name;
            message = message || {};
            message.fn = name;
        } else {
            callback = message;
            message = name;
        }

        var id = SlamWorker.instance.uuid();
        if (callback) this.callbacks[id] = callback;

        if (message.transfer) {
            message.msg.id = id;
            message.msg.fn = message.fn;
            message.msg.transfer = true;
            this.worker.postMessage(message.msg, message.buffer);
        } else {
            this.msg.message = message;
            this.msg.id = id;
            this.worker.postMessage(this.msg);
        }
    }
}
