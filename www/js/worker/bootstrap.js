SlamWorker.Bootstrap = `
self.SlamWorker = {};

SlamWorker.WASM_FILE = "http://localhost:3000/wasm/facelab.wasm"; //TODO: More flexibile

self.addEventListener('message', receiveMessage);

function receiveMessage(e) {
    if (e.data.es6) {
        const Processor = eval(e.data.es6);
        self.processor = new Processor();
        return;
    }

    if (e.data.importScript) {
        importScripts(e.data.path);
        return;
    }

    if (e.data.fn) {
        self.processor[e.data.fn](e.data, e.data.id);
        return;
    }

    if (e.data.message.fn) {
        self.processor[e.data.message.fn](e.data.message, e.data.id);
        return;
    }
}

function resolve(data, id, buffer) {
    if (!(data && id)) {
        id = data;
        data = null;
    }

    var message = {post: true, id: id, message: data};
    if (buffer) {
        for (var key in data) {
            message[key] = data[key];
            message.message[key] = message[key];
        }
        self.postMessage(message, buffer);
    } else {
        self.postMessage(message);
    }
}

`;
